
Zenophile Extras - A module for the Drupal content management system
by Elliott Rothman
My Drupal user page:
http://drupal.org/user/59521

This module's project page:
http://drupal.org/project/zenophile_extras

Zenophile Extras adds some additional theming jump starts to the already
excellent Zenophile module such as typography and coloring options.

It's not intended to be a complete theme building solution, but rather a way
to make high level design decisions before even touching the CSS, and save
even more time.

Note that this module is only intended to work with the 1.x branch of Zen. Once
the Zen 2.x branch reaches beta status, The process of tweaking
Zenophile so that it is compatible will begin, as will the process of tweaking
Zenophile Extras.

Usage:

Install the Zen theme, and Zenophile module, and install this module. The options
will automatically be made available on the zenophiles standard 'Create Zen
Subtheme' page.

Compatibility:
This module will not work with PHP4, and only with PHP5+

Development of this module is dedicated to Katharine.

Development of this module is sponsored by The SuperGroup. 
http://www.thesupergroup.com